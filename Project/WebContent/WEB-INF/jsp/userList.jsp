<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
	<body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">

                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="text-white">${userInfo.name}&nbsp;さん&emsp;</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-danger" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-auto mx-auto">
                    <h1><strong>ユーザ一覧</strong></h1>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="ml-auto">
                    <a class="link text-info" href="SignupServlet">新規登録</a>
                </div>
            </div>

			<form action="UserListServlet" method="post">
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-2">
                    ログインID
                </div>
                <div class="col-sm-7">
                    <input type="text" name="login-id" style="width:400px;" class="form-control">
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-2">
                    ユーザ名
                </div>
                <div class="col-sm-7">
                    <input type="text" name="user-name" style="width:400px;" class="form-control">
                </div>
            </div>
            <br>

            <div class="form-group row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-2">
                    生年月日
                </div>
                <div class="col-sm-7 form-inline">
                    <input type="date" name="date-start" style="width:188px;" class="form-control">
                    &nbsp;～&nbsp;
                    <input type="date" name="date-end" style="width:188px;" class="form-control">
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-9">
                </div>
                <div class="col-auto mx-auto">
                    <button type="submit" style="width:200px;" class="btn btn-secondary">検索</button>
                </div>
            </div>
            </form>
            <hr>
            <br>
            <div class="row">
                <div class="col-auto mx-auto">
                    <table class="table-bordered">

                            <tr class="table-secondary">
                                <th style="width:270px;" class="text-center">ログインID</th>
                                <th style="width:270px;" class="text-center">ユーザ名</th>
                                <th style="width:270px;" class="text-center">生年月日</th>
                                <th style="width:300px;"></th>
                            </tr>
							<c:forEach var="user" items="${userList}">
                            <tr>
                            <c:if test="${user.loginId != 'admin'}">
                                <th>${user.loginId}</th>
                                <th>${user.name}</th>
                                <th>${user.birthDate}</th>
                                <th>
                                    <div class="row">
                                        <div class="col-auto"></div>
                                        <div class="col-auto">
                                            <a class="btn btn-primary" href="ViewDetailServlet?id=${user.id}" role="button">詳細</a>
                                        </div>
                                        <c:if test="${userInfo.loginId == user.loginId || userInfo.loginId == 'admin'}">
                                        	<div class="col-auto">
                                            	<a class="btn btn-success" href="UpdateServlet?id=${user.id}" role="button">更新</a>
                                        	</div>
                                        </c:if>
                                        <c:if test="${userInfo.loginId == 'admin'}">
                                        	<div class="col-auto">
                                            	<a class="btn btn-danger" href="DeleteServlet?id=${user.id}" role="button">削除</a>
                                        	</div>
                                        </c:if>
                                        <div class="col-auto"></div>
                                    </div>
                                </th>
                             </c:if>
                            </tr>
							</c:forEach>
                    </table>
                </div>
            </div>
        </div>
	</body>
</html>
