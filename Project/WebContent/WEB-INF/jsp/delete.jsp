<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ削除確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
	<body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">

                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="text-white">${userInfo.name}&nbsp;さん&emsp;</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-danger" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-auto mx-auto">
                    <h1><strong>ユーザ削除確認</strong></h1>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-auto mx-auto">
                    ユーザID：${user.loginId}
                    <br>
                    を本当に削除してよろしいでしょうか。
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="ml-auto">
                    <a class="btn btn-secondary" style="width:200px;" href="UserListServlet" role="button">キャンセル</a>
                </div>

	            <div class="mx-auto">
	               	<form action="DeleteServlet?lId=${user.loginId}" method="post">
	                   	<button type="submit" style="width:200px;" class="btn btn-secondary">OK</button>
	               	</form>
	            </div>

            </div>
        </div>
	</body>
</html>
