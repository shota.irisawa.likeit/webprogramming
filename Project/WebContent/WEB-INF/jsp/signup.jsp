<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
	<body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">

                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="text-white">${userInfo.name}&nbsp;さん&emsp;</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-danger" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-auto mx-auto">
                    <h1><strong>ユーザ新規登録</strong></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-auto mx-auto text-danger">
                    ${errMsg}
                </div>
            </div>

            <br>
			<form action="SignupServlet" method="post">
	            <div class="row">
	                <div class="col-sm-3">
	                </div>
	                <div class="col-sm-3">
	                    ログインID
	                </div>
	                <div class="col-sm-6">
	                    <input type="text" name="login-id" style="width:300px;" class="form-control">
	                </div>
	            </div>
	            <br>

	            <div class="row">
	                <div class="col-sm-3">
	                </div>
	                <div class="col-sm-3">
	                    パスワード
	                </div>
	                <div class="col-sm-6">
	                    <input type="password" name="password" style="width:300px;" class="form-control">
	                </div>
	            </div>
	            <br>

	            <div class="row">
	                <div class="col-sm-3">
	                </div>
	                <div class="col-sm-3">
	                    パスワード（確認）
	                </div>
	                <div class="col-sm-6">
	                    <input type="password" name="password-v" style="width:300px;" class="form-control">
	                </div>
	            </div>
	            <br>

	            <div class="row">
	                <div class="col-sm-3">
	                </div>
	                <div class="col-sm-3">
	                    ユーザ名
	                </div>
	                <div class="col-sm-6">
	                    <input type="text" name="user-name" style="width:300px;" class="form-control">
	                </div>
	            </div>
	            <br>

	            <div class="row">
	                <div class="col-sm-3">
	                </div>
	                <div class="col-sm-3">
	                    生年月日
	                </div>
	                <div class="col-sm-6">
	                    <input type="date" name="birth-date" style="width:300px;" class="form-control">
	                </div>
	            </div>
	            <br>
	            <br>
	            <div class="row">
	                <div class="col-auto mx-auto">
	                    <button type="submit" style="width:200px;" class="btn btn-secondary">登録</button>
	                </div>
	            </div>

	        </form>
            <br>
            <br>
            <div class="row">
                <div class="mr-auto">
                    <a class="link text-info" href="UserListServlet">戻る</a>
                </div>
            </div>
        </div>
	</body>
</html>
