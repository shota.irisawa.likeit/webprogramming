package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import model.User;

public class UserDao {

	//ログインIDとパスワードが一致するユーザデータを検索する
	public User findByLoginInfo(String loginId, String password) {

	    Connection conn = null;
	        try {
	            //データベースへ接続
	        	conn = DBManager.getConnection();

	        	//実行するSQL文。引数をそれぞれ代入し検索を行う
	        	String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

	        	PreparedStatement pStmt = conn.prepareStatement(sql);
	        	pStmt.setString(1, loginId);
	        	pStmt.setString(2, password);
	        	ResultSet rs = pStmt.executeQuery();

	        	//該当するデータが存在しない場合nullを戻り値として返す
	        	if(!rs.next()) {
	        		return null;
	        	}

	        	//該当するデータが存在する場合ログインIDと名前を戻り値として返す
	        	String loginIdData = rs.getString("login_id");
	        	String nameData = rs.getString("name");

	        	return new User(loginIdData, nameData);


	        } catch (SQLException e) {
	        	e.printStackTrace();
	        	return null;

	        } finally {
	        	// データベース切断
	          if (conn != null) {
	          	try {
	          			conn.close();

	            	} catch (SQLException e) {
	                	e.printStackTrace();
	                	return null;

	            	}
	        	}
	        }

	}

	//IDをもとにユーザデータを検索する
	public User findByID(String id){

		Connection conn = null;
		try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	//実行するSQL文。引数を代入し検索を行う
        	String sql = "SELECT * FROM user WHERE id = ?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setString(1, id);
        	ResultSet rs = pStmt.executeQuery();

        	//該当するデータが存在しない場合nullを戻り値として戻す
        	if(!rs.next()) {
        		return null;
        	}

        	//該当するデータが存在する場合パスワードを除くユーザデータを戻り値として返す
        	int idData = rs.getInt("id");
        	String loginIdData = rs.getString("login_id");
        	String nameData = rs.getString("name");
        	Date birthDateData = rs.getDate("birth_date");
        	String createDateData = String.format("%.19s", rs.getString("create_date"));
        	String updateDateData = String.format("%.19s", rs.getString("update_date"));

        	return new User(idData, loginIdData, nameData, birthDateData, null, createDateData, updateDateData);


        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;

        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
          			conn.close();

            	} catch (SQLException e) {
                	e.printStackTrace();
                	return null;

            	}
        	}
        }

	}

	//すべてのユーザデータを検索しList型のコレクションとして返す
	public List<User> findAll(){

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql = "SELECT * FROM user";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}

		//コレクションを戻り値として返す
		return userList;

	}

	//ログインID、名前、生年月日の範囲をもとにユーザデータを検索
	//該当データをすべてList型のコレクションで返す
	public List<User> search(String lId, String uName, String dStart, String dEnd){

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文。ログインIDの入力有無に応じて検索条件を変える
			String sql;
			if(!(lId.equals(""))) {
				sql = "SELECT * FROM user WHERE login_id = ? AND name LIKE CONCAT ('%', ?, '%') AND birth_date >= ? AND birth_date < ?";
			} else {
				sql = "SELECT * FROM user WHERE login_id LIKE CONCAT('%', ?, '%') AND name LIKE CONCAT ('%', ?, '%') AND birth_date >= ? AND birth_date < ?";

			}

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, lId);

			pStmt.setString(2, uName);

			//生年月日開始側が空欄の場合最小値を代入
			if(dStart.equals("")) {
				pStmt.setString(3, "0000-00-00");
			}else {
				pStmt.setString(3, dStart);
			}

			//生年月日終了側が空欄の場合最大値を代入
			if(dEnd.equals("")) {
				pStmt.setString(4, "9999-12-31");
			}else {
				pStmt.setString(4, dEnd);
			}

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return userList;

	}

	//ログインIDをもとにデータを検索。該当データが存在すればtrue、しなければfalseを返す
	public boolean findByLoginId(String lId) {

		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文。
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, lId);

			ResultSet rs = pStmt.executeQuery();

			return rs.next();


		} catch (SQLException e) {
			e.printStackTrace();
			return true;

		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return true;
				}
			}

		}

	}

	//引数と現在日時をもとにデータ登録処理を行う
	public void insert(String lId, String pWord, String uName, String bDate) {

		Connection conn = null;
		Calendar calendar = Calendar.getInstance();

		//現在日時の取得、フォーマット
		SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String nowDate = f.format(calendar.getTime());

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文。引数を代入し登録処理を行う
			String sql = "INSERT INTO user(" +
					"	login_id," +
					"	name," +
					"	birth_date," +
					"	password," +
					"	create_date," +
					"	update_date) VALUES (" +
					"	?," +
					"	?," +
					"	?," +
					"	?," +
					"	?," +
					"	?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, lId);
			pStmt.setString(2, uName);
			pStmt.setString(3, bDate);
			pStmt.setString(4, pWord);
			pStmt.setString(5, nowDate);
			pStmt.setString(6, nowDate);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();


		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
	}

	//ログインIDをもとにデータ削除処理を行う
	public void delete(String lId) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文。データ削除処理
			String sql = "DELETE FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, lId);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}

	}

	//引数と現在日時をもとにデータ更新処理を行う
	public void update(boolean passFlg, String lId, String pWord, String uName, String bDate) {

		Calendar calendar = Calendar.getInstance();

		//現在日時の取得、フォーマット
		SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String nowDate = f.format(calendar.getTime());

		//passFlgがtrueのときパスワードを含めて更新処理を行う
		if(passFlg == true) {

			Connection conn = null;

			try {
				//データベースへ接続
				conn = DBManager.getConnection();

				//実行するSQL文。データ更新処理
				String sql = "UPDATE user SET name = ?,"
						+ " birth_date = ?,"
						+ " password = ?,"
						+ " update_date = ?"
						+ " WHERE login_id = ?";
				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, uName);
				pStmt.setString(2, bDate);
				pStmt.setString(3, pWord);
				pStmt.setString(4, nowDate);
				pStmt.setString(5, lId);

				pStmt.executeUpdate();


			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

				if(conn != null) {
					try {
						//データベース切断
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}

			}

		}

		//passFlgがfalseのときパスワードを除いて更新処理を行う
		if(passFlg == false) {

			Connection conn = null;

			try {
				//データベースへ接続
				conn = DBManager.getConnection();

				//実行するSQL文。データ更新処理
				String sql = "UPDATE user SET name = ?,"
						+ " birth_date = ?,"
						+ " update_date = ?"
						+ " WHERE login_id = ?";
				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, uName);
				pStmt.setString(2, bDate);
				pStmt.setString(3, nowDate);
				pStmt.setString(4, lId);

				pStmt.executeUpdate();


			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

				if(conn != null) {
					try {
						//データベース切断
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}

			}

		}
	}

}
