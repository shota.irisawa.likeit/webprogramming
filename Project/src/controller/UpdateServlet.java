package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Epass;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ログインセッションが存在しない場合「ログイン画面」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//ログインセッションが存在する場合
			//フォームからIDを取得
			String id = request.getParameter("id");

			//IDをもとにユーザデータを取得
			UserDao userDao = new UserDao();
			User user = userDao.findByID(id);

			//エラーメッセージを初期化、ユーザデータをセットし「ユーザ情報更新」へフォワード
			request.setAttribute("errMsg", null);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォームからIDを取得
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();

		//IDをもとにユーザデータ(特にログインID)を取得
		User user = userDao.findByID(id);
		String loginId = user.getLoginId();

		//フォームの入力情報を取得
		String password = request.getParameter("password");
		String passwordV = request.getParameter("password-v");
		String userName = request.getParameter("user-name");
		String birthDate = request.getParameter("birth-date");

		//入力されたパスワードを暗号化
		Epass epass = new Epass();
		String pass = epass.encryption(password);

		//パスワード、パスワード(確認用)がともに空欄の場合のみ更新の処理からパスワードの項目を除く
		boolean passFlg;
		if(password.equals("") && passwordV.equals("")) {
			passFlg = false;
		} else {
			passFlg = true;
		}

		//パスワード、パスワード(確認用)以外に空欄が存在せずかつパスワードとパスワード(確認用)が一致している場合のみ
		//該当する項目の更新処理を行い、「ユーザ一覧」サーブレットへリダイレクト
		//条件を満たさない場合エラーメッセージとユーザデータをセットし「ユーザ情報更新」へフォワード
		if(!(userName.equals("") || birthDate.equals("")) && password.equals(passwordV)) {
			userDao.update(passFlg, loginId, pass, userName, birthDate);
			response.sendRedirect("UserListServlet");
		} else {
			request.setAttribute("errMsg", "入力された内容が正しくありません");
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
		}

	}

}
