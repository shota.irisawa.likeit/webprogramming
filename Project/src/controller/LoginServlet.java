package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Epass;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//エラーメッセージを初期化し「ログイン画面」へフォワード
		request.setAttribute("errMsg", null);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		//フォームの入力情報を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		//入力されたパスワードを暗号化
		Epass epass = new Epass();
		String pass = epass.encryption(password);

		//入力情報をデータベースと照合
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(loginId, pass);

		//該当データが存在する場合ログイン処理を行う
		//存在しない場合エラーメッセージをセットし、「ログイン画面」へフォワード
		if(user == null) {
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログインセッションにユーザデータをセットし「ユーザ一覧」サーブレットへリダイレクト
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		response.sendRedirect("UserListServlet");
	}

}
