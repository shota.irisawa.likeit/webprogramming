package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class ViewDetailServlet
 */
@WebServlet("/ViewDetailServlet")
public class ViewDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ログインセッションが存在しない場合「ログイン画面」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//ログインセッションが存在する場合
			//フォームからIDを取得
			String id = request.getParameter("id");

			//IDをもとにユーザ情報を取得
			UserDao userDao = new UserDao();
			User user = userDao.findByID(id);

			//ユーザ情報をセットし「ユーザ情報詳細参照」へフォワード
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/viewDetails.jsp");
			dispatcher.forward(request, response);

		}


	}

}
