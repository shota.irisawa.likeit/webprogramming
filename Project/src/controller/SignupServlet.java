package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Epass;

/**
 * Servlet implementation class SignupServlet
 */
@WebServlet("/SignupServlet")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ログインセッションが存在しない場合「ログイン画面」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//ログインセッションが存在する場合
			//エラーメッセージを初期化し「ユーザ新規登録」へフォワード
			request.setAttribute("errMsg", null);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		UserDao userDao = new UserDao();

		//フォームの入力情報を取得
		String loginId = request.getParameter("login-id");
		String password = request.getParameter("password");
		String passwordV = request.getParameter("password-v");
		String userName = request.getParameter("user-name");
		String birthDate = request.getParameter("birth-date");

		//入力されたパスワードを暗号化
		Epass epass = new Epass();
		String pass = epass.encryption(password);

		//ログインIDの重複を判定
		boolean dCheck = userDao.findByLoginId(loginId);

		//フォームに空欄が存在せずかつ入力されたログインIDに既存のログインIDとの重複が無い場合のみ新規登録処理を行う。
		//条件を満たさない場合エラーメッセージをセットし「ユーザ新規登録」へフォワード
		if(!(loginId.equals("") || password.equals("") || userName.equals("") || birthDate.equals(""))) {

			if(password.equals(passwordV) && dCheck == false) {
				userDao.insert(loginId, pass, userName, birthDate);
				response.sendRedirect("UserListServlet");
			} else {
				request.setAttribute("errMsg", "入力された内容が正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
				dispatcher.forward(request, response);
			}

		} else {
			request.setAttribute("errMsg", "入力された内容が正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
		}

	}

}
