package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ログインセッションが存在しない場合「ログイン画面」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//ログインセッションが存在する場合
			//すべてのユーザデータを取得、セットし「ユーザ一覧」へフォワード
			UserDao userDao = new UserDao();
			List<User> userList = userDao.findAll();

			request.setAttribute("userList", userList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
			dispatcher.forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォームの入力情報を取得
		String loginId = request.getParameter("login-id");
		String userName = request.getParameter("user-name");
		String dateStart = request.getParameter("date-start");
		String dateEnd = request.getParameter("date-end");

		//入力された情報に該当するデータを検索、取得、セットし「ユーザ一覧」へフォワード
		UserDao userDao = new UserDao();
		List<User> userList = userDao.search(loginId, userName, dateStart, dateEnd);

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

}